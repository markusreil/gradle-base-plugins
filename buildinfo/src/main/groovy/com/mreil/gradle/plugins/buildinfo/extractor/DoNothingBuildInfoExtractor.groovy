package com.mreil.gradle.plugins.buildinfo.extractor

class DoNothingBuildInfoExtractor extends AbstractBuildInfoExtractor {
    @Override
    String getBranch() {
        return null
    }

    @Override
    String getRevision() {
        return "revision"
    }

    @Override
    Collection<String> getTags() {
        return null
    }

    @Override
    protected void extract() {
        //
    }
}
