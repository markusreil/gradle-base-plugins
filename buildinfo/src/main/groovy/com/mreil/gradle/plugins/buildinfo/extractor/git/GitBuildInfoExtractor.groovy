package com.mreil.gradle.plugins.buildinfo.extractor.git

import com.mreil.gradle.plugins.buildinfo.extractor.AbstractBuildInfoExtractor
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class GitBuildInfoExtractor extends AbstractBuildInfoExtractor {
    private final Logger log = LoggerFactory.getLogger(GitBuildInfoExtractor.class)
    private String branch
    private String revision
    private Set<String> tags
    private final File dir

    GitBuildInfoExtractor(File gitDir) {
        this.dir = gitDir
    }

    @Override
    String getBranch() {
        return branch
    }

    @Override
    String getRevision() {
        return revision
    }

    @Override
    Collection<String> getTags() {
        return tags
    }

    @Override
    protected void extract() {
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        Repository repository = builder.setGitDir(dir)
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .build();

        branch = repository.branch
        revision = repository.getRef(branch).objectId.abbreviate(6).name()
        tags = repository.tags.keySet()

        repository.close()

    }
}
