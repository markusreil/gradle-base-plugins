package com.mreil.gradle.plugins.buildinfo.extractor
import com.mreil.gradle.plugins.buildinfo.model.BuildInfo

abstract class AbstractBuildInfoExtractor implements BuildInfoExtractor {
    protected abstract String getBranch();
    protected abstract String getRevision();
    protected abstract Collection<String> getTags();
    protected abstract void extract();

    public BuildInfo getBuildInfo() {
        extract();
        return new BuildInfo(getRevision(),
                getTags(),
                getBranch());
    }
}
