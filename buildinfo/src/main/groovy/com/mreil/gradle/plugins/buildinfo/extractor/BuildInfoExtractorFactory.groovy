package com.mreil.gradle.plugins.buildinfo.extractor

import com.mreil.gradle.plugins.buildinfo.extractor.git.GitBuildInfoExtractor

class BuildInfoExtractorFactory {
    private final File projectDir

    BuildInfoExtractorFactory(File projectDir) {
        this.projectDir = projectDir
        if (!projectDir.isDirectory()) {
            throw new IllegalArgumentException("file must be a directory")
        }
    }

    public BuildInfoExtractor getExtractor() {
        final BuildInfoExtractor extractor

        def gitDir = new File(projectDir.path, ".git")
        if (gitDir.exists()) {
            extractor = new GitBuildInfoExtractor(gitDir)
        } else {
            extractor = new DoNothingBuildInfoExtractor()
        }

        return extractor
    }

}
