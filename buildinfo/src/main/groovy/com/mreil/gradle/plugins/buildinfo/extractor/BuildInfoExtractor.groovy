package com.mreil.gradle.plugins.buildinfo.extractor

import com.mreil.gradle.plugins.buildinfo.model.BuildInfo

interface BuildInfoExtractor {
    BuildInfo getBuildInfo()
}
