package com.mreil.gradle.plugins.buildinfo.model

class BuildInfo {
    final Date date = new Date()
    final String revision
    final Collection<String> tags
    final String branch

    BuildInfo(String revision, Collection<String> tags, String branch) {
        this.revision = revision
        this.tags = tags
        this.branch = branch
    }

    @Override
    public String toString() {
        return "BuildInfo{" +
                "date=" + date +
                ", revision='" + revision + '\'' +
                ", tags=" + tags +
                ", branch='" + branch + '\'' +
                '}';
    }
}
