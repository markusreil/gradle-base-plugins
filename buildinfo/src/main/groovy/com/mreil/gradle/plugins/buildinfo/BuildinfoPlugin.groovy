package com.mreil.gradle.plugins.buildinfo
import com.mreil.gradle.plugins.buildinfo.extractor.BuildInfoExtractor
import com.mreil.gradle.plugins.buildinfo.extractor.BuildInfoExtractorFactory
import com.mreil.gradle.plugins.buildinfo.model.BuildInfo
import com.mreil.gradle.plugins.buildinfo.model.BuildinfoExtension
import com.mreil.gradle.plugins.buildinfo.tasks.BuildinfoTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class BuildinfoPlugin implements Plugin<Project> {
    @Override
    void apply(Project target) {
        BuildinfoExtension ext = target.extensions.create("buildinfo", BuildinfoExtension)
        ext.outputDir = new File(target.buildDir, "buildinfo")
        ext.outputDir.mkdirs()

        createAndWireTask(target)

        BuildInfoExtractor ex = new BuildInfoExtractorFactory(target.projectDir).extractor
        BuildInfo info = ex.getBuildInfo();

        // make available in build
        ext.buildInfo = info
    }

    private static void createAndWireTask(Project target) {
        def buildinfoTask = target.task(BuildinfoTask.NAME, type: BuildinfoTask)
        if (target.tasks.hasProperty("processResources")) {
            target.processResources.dependsOn(buildinfoTask)
        }
    }

}
