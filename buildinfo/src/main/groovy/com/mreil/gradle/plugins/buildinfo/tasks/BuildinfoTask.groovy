package com.mreil.gradle.plugins.buildinfo.tasks
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.mreil.gradle.plugins.buildinfo.model.BuildinfoExtension
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.format.DateTimeFormat

class BuildinfoTask extends DefaultTask {
    public static final String NAME = 'buildinfo'

    @TaskAction
    public void task() {
        BuildinfoExtension ext = getProject().buildinfo
        File dir = ext.outputDir

        def (File metainf, File resources) = mkdirs(dir)
        writeBuildInfoJson(metainf, ext)
        addToSourceSets(resources)
        setProjectVersion(ext)
    }

    private void setProjectVersion(BuildinfoExtension ext) {
        def date = DateTimeFormat.forPattern("yyyyMMdd").print(new DateTime(DateTimeZone.UTC))
        project.version = "${date}-${ext.buildInfo.revision.substring(0, 6)}"
        logger.warn("Setting project.version to ${project.version}")
    }

    private void addToSourceSets(File resources) {
        if (project.hasProperty("sourceSets")) {
            project.sourceSets?.main?.resources {
                srcDir resources
            }
        }
    }

    private void writeBuildInfoJson(File metainf, BuildinfoExtension ext) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create()
        File info = new File(metainf, "buildinfo.json")
        logger.info("Writing buildinfo to ${info}")
        info.write(gson.toJson(ext.buildInfo))
    }

    private List mkdirs(File dir) {
        File resources = new File(dir, "resources")
        resources.mkdirs()
        File metainf = new File(resources, "META-INF")
        metainf.mkdirs()
        [metainf, resources]
    }

}
