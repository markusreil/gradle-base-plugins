package com.mreil.gradle.plugins.buildinfo
import com.mreil.gradle.plugins.buildinfo.model.BuildInfo
import com.mreil.gradle.plugins.buildinfo.model.BuildinfoExtension
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.ReflogEntry
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification

import static org.hamcrest.Matchers.emptyCollectionOf
import static org.hamcrest.Matchers.hasItems
import static org.hamcrest.core.Is.is
import static org.hamcrest.core.IsNull.notNullValue
import static org.junit.Assert.assertThat

class BuildinfoPluginSpec extends Specification {

    public static final String BRANCH = "mybranch"

    @Shared
    Project project

    @Shared
    Git git

    def setup() {
        // init project
        project = ProjectBuilder.builder().build()
        project.plugins.apply("java")

        // init repo
        git = Git.init().setDirectory(project.projectDir).call();
        def f = project.file("test.txt")
        f.write("sometext")
        git.add().addFilepattern("*.*").call()
        git.commit().setMessage("initial commit").call()
        git.branchCreate().setName(BRANCH).call()
    }

    def cleanup() {
        git.close()
    }

    def "Use plugin in project"() {
        when: "The plugin is applied"
        project.pluginManager.apply("com.mreil.buildinfo")

        then: "The project has buildinfo property"
        BuildinfoExtension ext = project.buildinfo
        BuildInfo buildinfo = ext.buildInfo
        assertThat(buildinfo, notNullValue())

        and: "The field values are correct"
        Collection<ReflogEntry> rl = git.reflog().call()
        def rev = rl.get(0).newId.abbreviate(6).name()
        assertThat(buildinfo.revision, is(rev))
        assertThat(buildinfo.date, notNullValue())
        assertThat(buildinfo.branch, is("master"))
        assertThat(buildinfo.tags, emptyCollectionOf(String.class))

        and: "buildinfo task is available"
        assertThat(project.tasks.getByName("buildinfo"), notNullValue())

    }

    def "Use plugin in project on a branch"() {
        when: "A branch is checked out"
        git.checkout().setName(BRANCH).call()

        and: "The plugin is applied"
        project.pluginManager.apply("com.mreil.buildinfo")

        then: "The branch name is correct"
        def buildinfo = (BuildInfo) project.buildinfo.buildInfo
        assertThat(buildinfo.branch, is(BRANCH))
    }

    def "Use plugin in project on a tag"() {
        when: "A tag is checked out"
        git.tag().setName("atag").call()

        and: "The plugin is applied"
        project.pluginManager.apply("com.mreil.buildinfo")

        then: "The tag names are correct"
        def buildinfo = (BuildInfo) project.buildinfo.buildInfo
        assertThat(buildinfo.tags, hasItems("atag"))
    }

}
