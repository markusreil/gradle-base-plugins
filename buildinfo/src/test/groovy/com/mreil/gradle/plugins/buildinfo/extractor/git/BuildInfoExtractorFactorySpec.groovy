package com.mreil.gradle.plugins.buildinfo.extractor.git

import com.mreil.gradle.plugins.buildinfo.extractor.BuildInfoExtractorFactory
import com.mreil.gradle.plugins.buildinfo.extractor.DoNothingBuildInfoExtractor
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Shared
import spock.lang.Specification

class BuildInfoExtractorFactorySpec extends Specification {
    @Shared
    File projectDir

    def setup() {
        // init project
        def project = ProjectBuilder.builder().build()
        projectDir = project.buildDir
        projectDir.mkdirs()
    }

    def "Invalid dir"() {
        when: "A factory is created with an invalid project directory"
        new BuildInfoExtractorFactory(new File(projectDir, "bla"))

        then:
        thrown(IllegalArgumentException)
    }

    def "Git extractor"() {
        when: "Git directory"
        def dir = new File(projectDir, ".git")
        dir.mkdirs()

        then:
        def ex = new BuildInfoExtractorFactory(projectDir).extractor
        ex instanceof GitBuildInfoExtractor
    }

    def "Do-nothing extractor"() {
        when: "No git directory"

        then:
        def ex = new BuildInfoExtractorFactory(projectDir).extractor
        ex instanceof DoNothingBuildInfoExtractor
    }
}
