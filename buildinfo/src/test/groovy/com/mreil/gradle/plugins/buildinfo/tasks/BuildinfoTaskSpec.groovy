package com.mreil.gradle.plugins.buildinfo.tasks
import groovy.json.JsonSlurper
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.hamcrest.Matchers
import org.joda.time.format.DateTimeFormat
import spock.lang.Shared
import spock.lang.Specification

import static org.hamcrest.CoreMatchers.notNullValue
import static org.junit.Assert.assertThat
import static org.junit.Assert.assertTrue

class BuildinfoTaskSpec extends Specification {

    @Shared
    Project project

    def setup() {
        // init project
        project = ProjectBuilder.builder().build()
        project.pluginManager.apply("com.mreil.buildinfo")
    }

    def "Call task"() {
        when: "The task is executed"
        def task = project.tasks.getByName(BuildinfoTask.NAME)
        task.actions.each { Action a ->
            a.execute(task)
        }

        then: "The output directory exists"
        File dir = new File(project.buildDir, "buildinfo")
        assertTrue("Directory 'buildinfo' not found in ${project.buildDir}: ${project.buildDir.listFiles()}", dir.exists())

        and: "The buildinfo.json file exists"
        def json = new File(dir.path + "/resources/META-INF/buildinfo.json")
        assertTrue("File not found in ${}", json.exists())

        and: "The file is valid json and contains a date property"
        def info = new JsonSlurper().parse(json)
        assertThat(info.date, notNullValue())
    }

    def "Call task with Java plugin enabled"() {
        when: "The java plugin is applied"
        project.plugins.apply("java")

        and: "The task is executed"
        def task = project.tasks.getByName(BuildinfoTask.NAME)
        task.actions.each { Action a ->
            a.execute(task)
        }

        then: "The additional dir is included in the resources"
        assertThat(project.sourceSets.main.resources.source.size(), Matchers.is(2))
    }

    def "Project version is set"() {
        when: "The task is executed"
        def task = project.tasks.getByName(BuildinfoTask.NAME)
        task.actions.each { Action a ->
            a.execute(task)
        }

        then: "The project version is set"
        def date = DateTimeFormat.forPattern("yyyyMMdd").print(project.buildinfo.buildInfo.date.time)
        assertThat(project.version, Matchers.is("${date}-revisi"))
    }

}
