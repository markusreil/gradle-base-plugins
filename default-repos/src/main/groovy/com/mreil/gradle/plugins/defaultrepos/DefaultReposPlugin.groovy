package com.mreil.gradle.plugins.defaultrepos

import org.gradle.api.Plugin
import org.gradle.api.Project

class DefaultReposPlugin implements Plugin<Project> {

    private static final Closure REPOS = {
        mavenLocal()
        jcenter()
    }

    @Override
    void apply(Project project) {
        project.repositories REPOS
        project.subprojects.each { subproject ->
            subproject.repositories REPOS
        }
    }

}
