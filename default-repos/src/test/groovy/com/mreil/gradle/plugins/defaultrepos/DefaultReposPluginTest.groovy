package com.mreil.gradle.plugins.defaultrepos
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

import static org.hamcrest.Matchers.hasItem
import static org.junit.Assert.assertThat

class DefaultReposPluginSpec extends Specification {

    def "Use plugin in single-module project"() {
        setup: "Create Gradle build file with applied plugin"
        Project project = ProjectBuilder.builder().build()

        when: "The plugin is applied"
        project.pluginManager.apply("com.mreil.default-repos")

        then: "The default repositories are part of the project repositories"
        def repos = project.repositories.collect { it.name }
        assertThat(repos, hasItem("MavenLocal"))
        assertThat(repos, hasItem("BintrayJCenter"))
    }

    def "Use plugin in multi-module project"() {
        setup: "Create Gradle build file with applied plugin"
        Project parent = ProjectBuilder.builder().build()
        Project childProject = ProjectBuilder.builder().withParent(parent).build()

        when: "The plugin is applied"
        parent.pluginManager.apply("com.mreil.default-repos")

        then: "The default repositories are part of the parent repositories"
        def parentRepos = parent.repositories.collect { it.name }
        assertThat(parentRepos, hasItem("MavenLocal"))
        assertThat(parentRepos, hasItem("BintrayJCenter"))

        and: "The repositories are part of the child repositories"
        def childRepos = childProject.repositories.collect { it.name }
        assertThat(childRepos, hasItem("MavenLocal"))
        assertThat(childRepos, hasItem("BintrayJCenter"))
    }

}
