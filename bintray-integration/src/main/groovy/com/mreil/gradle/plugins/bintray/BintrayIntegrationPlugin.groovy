package com.mreil.gradle.plugins.bintray

import com.google.common.base.Function
import com.google.common.base.Optional
import org.gradle.api.Plugin
import org.gradle.api.Project

class BintrayIntegrationPlugin implements Plugin<Project> {
    @Override
    void apply(Project target) {
        target.allprojects.each { p ->
            applyBintraySettings(p)
        }
    }

    void applyBintraySettings(Project project) {
        File[] directories = [project.projectDir,
                              project.parent?.projectDir,
                              project.gradle.gradleUserHomeDir]

        def file = findFirstInDirectories("bintray.properties", directories)

        project.afterEvaluate { p ->
            if (p.extensions.findByName("bintray")) {
                applyToProject(p, file)
                if (!p.bintray.pkg.repo) {
                    throw new IllegalStateException("no repo defined")
                }
            }
        }
    }

    def applyToProject(Project project, Optional<File> file) {
        applyCredentials(project, file)
        applyDefaultProperties(project)
    }

    def applyDefaultProperties(Project project) {
        def bintray = project.bintray
        bintray.publications = ["mavenJava"]
        bintray.dryRun = false
        bintray.publish = true
        bintray.pkg {
            name = project.archivesBaseName
            desc = project.description
            licenses = ['Apache-2.0']
            version {
                name = project.version
            }
        }
    }

    static def applyCredentials(Project p, Optional<File> f) {
        f.transform(
                new Function<File, Object>() {
                    @Override
                    Object apply(File file) {
                        p.apply(from: file, to: p.bintray)
                        p.bintray
                    }
                }
        )
    }

    static Optional<File> findFirstInDirectories(String filename, File... dirs) {
        for (File dir : dirs) {
            if (dir == null) continue
            def f = new File(dir, filename)
            if (f.exists()) {
                return Optional.of(f)
            }
        }
        return Optional.absent()
    }

}
