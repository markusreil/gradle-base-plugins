package com.mreil.gradle.plugins.bintray

import com.jfrog.bintray.gradle.BintrayPlugin
import org.gradle.api.Project
import org.gradle.api.ProjectConfigurationException
import org.gradle.testfixtures.ProjectBuilder
import org.hamcrest.Matchers
import spock.lang.Shared
import spock.lang.Specification

import static org.hamcrest.core.IsNull.notNullValue
import static org.junit.Assert.assertThat

class BintrayIntegrationPluginTest extends Specification {
    @Shared
    Project project

    def setup() {
        // init project
        project = ProjectBuilder.builder().build()
        project.plugins.apply("maven")
        project.plugins.apply(BintrayPlugin.class)
        new File(project.projectDir, "bintray.properties").write("""
                                user='abc'
                                key='meh'
                                """)
    }

    def "Bintray is initialised correctly with a properties file"() {
        when: "The project is applied"
        project.plugins.apply("com.mreil.bintray-integration")
        project.bintray.pkg.repo = "repo"
        project.evaluate()

        then:
        assertThat(project.bintray.user, Matchers.is('abc'))
    }

    def "Bintray is initialised correctly without a properties file"() {
        when: "No properties file exists"
        new File(project.projectDir, "bintray.properties").delete()

        and: "The plugin is applied"
        project.plugins.apply("com.mreil.bintray-integration")

        then:
        assertThat(project.bintray, notNullValue())
    }

    def "Error when repo is not defined"() {
        when: "The project is evaluated"
        project.plugins.apply("com.mreil.bintray-integration")
        project.evaluate()

        then:
        thrown(ProjectConfigurationException.class)
    }

    def "No error when repo is defined"() {
        when: "The project is evaluated"
        project.plugins.apply("com.mreil.bintray-integration")
        project.bintray.pkg.repo = "repo"
        project.evaluate()

        then:
        project.bintray.pkg.repo
    }

}
