# gradle-base-plugins

## default-repos

Automatically adds the local Maven repository and [JCenter](https://bintray.com/bintray/jcenter) to all modules.

    repositories {
        mavenLocal()
        jcenter()
    }

## Build Status

[![Build Status](https://semaphoreci.com/api/v1/projects/da3c33e2-f540-4ef6-9213-02c9864bd06e/393230/badge.svg)](https://semaphoreci.com/markusreil/gradle-base-plugins)
