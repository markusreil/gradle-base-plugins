package com.mreil.gradle.plugins.baseplugins

import org.apache.commons.io.FileUtils
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.hamcrest.Matchers
import spock.lang.Shared
import spock.lang.Specification

import static junit.framework.TestCase.assertTrue

import static org.junit.Assert.assertThat

class BasePluginsPluginSpec extends Specification {
    @Shared
    Project project

    def setup() {
        // init project
        project = ProjectBuilder.builder().build()
    }

    def "All plugins are loaded"() {
        when: "The plugin is applied"
        project.pluginManager.apply("com.mreil.baseplugins")

        then: "All plugins are applied"

        String[] extensions = ["properties"]

        def parseRoot = new File("..")
        println "Looking for plugins in ${parseRoot.absolutePath}"
        def plugins = FileUtils.listFiles(parseRoot,
                extensions,
                true)
                .findAll { File f -> f.path.contains("src/main/resources/META-INF/gradle-plugins/com.mreil") }
                .collect { File f -> f.path.replaceAll(".*/(.*).properties", '$1') }

        plugins.each {
            assertTrue("Plugin $it is not active, complete list: ${plugins}", project.plugins.hasPlugin(it))
        }
    }

    def "Jacoco is loaded"() {
        when: "The plugin is applied and the java plugin is applied"
        project.pluginManager.apply("java")
        project.pluginManager.apply("com.mreil.baseplugins")

        then: "The Jacoco plugin is applied"
        assertTrue(project.plugins.hasPlugin("jacoco"))
        assertThat(project.jacoco.toolVersion, Matchers.is("0.7.4.201502262128"))
    }

}
