package com.mreil.gradle.plugins.baseplugins

import org.gradle.api.Plugin
import org.gradle.api.Project

class BasePluginsPlugin implements Plugin<Project> {
    @Override
    void apply(Project target) {

        def mi = this.class.getClassLoader().getResourceAsStream("META-INF/gradle-plugins/all-base-plugins.txt")

        BufferedReader reader = new BufferedReader(new InputStreamReader(mi))

        String line
        while ((line = reader.readLine()) != null) {
            target.plugins.apply(line)
        }

        applyJacoco(target)

        reader.close()
        mi.close()
    }

    def applyJacoco(Project project) {
        project.allprojects.each { p ->
            if (p.plugins.hasPlugin("java")) {
                p.plugins.apply("jacoco")

                p.jacoco {
                    toolVersion = "0.7.4.201502262128"
                }
                p.jacocoTestReport {
                    reports {
                        xml.enabled false
                        csv.enabled false
                        html.destination "${p.buildDir}/reports/jacocoHtml"
                    }
                }
                p.build.dependsOn p.jacocoTestReport
            }
        }
    }

}
