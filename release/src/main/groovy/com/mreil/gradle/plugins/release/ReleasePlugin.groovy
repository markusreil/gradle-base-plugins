package com.mreil.gradle.plugins.release
import com.mreil.gradle.plugins.buildinfo.BuildinfoPlugin
import com.mreil.gradle.plugins.buildinfo.tasks.BuildinfoTask
import com.mreil.gradle.plugins.release.tasks.ReleaseInfoTask
import com.mreil.gradle.plugins.release.tasks.ReleaseTask
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task

class ReleasePlugin implements Plugin<Project> {

    @Override
    void apply(Project target) {
        target.plugins.apply(BuildinfoPlugin.class)
        target.extensions.create("release", ReleaseExtension.class)

        def releaseInfoTask = createAndWireBuildInfoTask(target.rootProject)

        target.allprojects.each {
            createAndWireTasks(it, releaseInfoTask)
        }
    }

    static def createAndWireBuildInfoTask(Project root) {
        def releaseInfoTask = root.tasks.findByName(ReleaseInfoTask.NAME)
        if (!releaseInfoTask) {
            releaseInfoTask = root.task(ReleaseInfoTask.NAME, type: ReleaseInfoTask)
            releaseInfoTask.dependsOn(BuildinfoTask.NAME)
        }
        return releaseInfoTask
    }

    static void createAndWireTasks(Project project, Task releaseInfoTask) {
        def releaseTask = project.task(ReleaseTask.NAME, type: ReleaseTask)
        releaseTask.dependsOn(releaseInfoTask)

        // if bintrayUpload exists then include it
        def bintrayUpload = project.tasks.findByName("bintrayUpload")
        if (bintrayUpload) {
            releaseTask.dependsOn(bintrayUpload)
            bintrayUpload.mustRunAfter(releaseInfoTask)
        }

        // add more release tasks here if needed
    }

}
