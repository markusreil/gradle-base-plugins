package com.mreil.gradle.plugins.release.tasks

import com.mreil.gradle.plugins.buildinfo.model.BuildInfo
import com.mreil.gradle.plugins.release.ReleaseExtension
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ReleaseInfoTask extends DefaultTask {
    public static final String NAME = "releaseInfo"
    public static final String RELEASE_BRANCH_PREFIX = "release/"

    @TaskAction
    public void task() {
        ReleaseExtension ext = project.release

        BuildInfo bi = project.buildinfo.buildInfo
        if (bi.branch?.startsWith(RELEASE_BRANCH_PREFIX)) {
            ext.release = true
            ext.tag = bi.branch.replace(RELEASE_BRANCH_PREFIX, "")
            project.version = ext.tag
        }
    }

}
