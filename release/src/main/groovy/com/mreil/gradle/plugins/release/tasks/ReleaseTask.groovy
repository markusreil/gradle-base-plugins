package com.mreil.gradle.plugins.release.tasks

import com.mreil.gradle.plugins.release.ReleaseExtension
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class ReleaseTask extends DefaultTask {
    public static final String NAME = "release"

    @TaskAction
    public void task() {
        ReleaseExtension ext = project.release

        if (ext.release) {
            doRelease()
        }
    }

    void doRelease() {
        // do additional stuff
    }
}
