package com.mreil.gradle.plugins.release

import com.mreil.gradle.plugins.release.tasks.ReleaseInfoTask
import org.eclipse.jgit.api.Git
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.hamcrest.Matchers
import spock.lang.Shared
import spock.lang.Specification

import static org.junit.Assert.assertThat

class ReleasePluginTest extends Specification {
    @Shared
    Project project

    def setup() {
        // init project
        project = ProjectBuilder.builder().build()
    }


    def "No release branch"() {
        when: "The project is evaluated"
        project.plugins.apply("com.mreil.release")
        project.evaluate()

        then:
        assertThat(project.release.release, Matchers.is(false))
    }

    def "Release branch"() {
        when: "On a release branch"
        // init repo
        Git git = Git.init().setDirectory(project.projectDir).call();
        def f = project.file("test.txt")
        f.write("sometext")
        git.add().addFilepattern("*.*").call()
        git.commit().setMessage("initial commit").call()
        def branch = "release/1.2.3"
        git.branchCreate().setName(branch).call()
        git.checkout().setName(branch).call()

        and: "The project is evaluated"
        project.plugins.apply("com.mreil.release")
        project.evaluate()
        execute(ReleaseInfoTask.NAME)

        then:
        assertThat(project.release.release, Matchers.is(true))
        assertThat(project.release.tag, Matchers.is("1.2.3"))
        assertThat(project.version, Matchers.is("1.2.3"))
    }

    def execute(String taskname) {
        def task = project.tasks.findByName(taskname)
        task.actions.get(0).execute(task)
    }

}
